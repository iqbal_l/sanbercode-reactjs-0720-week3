import React, { useState, useEffect  } from "react";
import axios from 'axios';
import {BuahProvider} from "./BuahContext"
import BuahList from "./BuahList"
import BuahForm from "./BuahForm"


const Tabel = () => {
  

    return (
      <BuahProvider>
       <BuahList/>
       <BuahForm/>
     </BuahProvider>

    )
}

    export default Tabel
