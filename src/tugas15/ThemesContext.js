import React, { useState, createContext } from "react";

export const ThemesContext = createContext();


export const ThemesProvider = props => {
  const [warna, setWarna] = useState("default");

  const handleWarna = (event) =>{
    let color = event.target.getAttribute('warna')
    let warnai = 'white'
    switch (color) {
      case 'default': warnai = "white"; break;
      case 'biru': warnai = "blue"; break;
      case 'hijau': warnai = "green"; break;
      default:break;
    }
    document.getElementById('content').style.background = warnai;
    setWarna(color)
  }


  return (
    <ThemesContext.Provider value={[handleWarna]}>
      {props.children}
    </ThemesContext.Provider>
  );
};
