import React, {useContext, useState} from "react"
import {BuahContext, BuahSelectedContext, BuahStatusFormContext, BuahInputContext} from "./BuahContext"
import axios from 'axios';

const BuahForm = () =>{
  const [buah, setBuah] = useContext(BuahContext)
  const [input, setInput] = useContext(BuahInputContext)
  const [selectedId, setSelectedId]  =  useContext(BuahSelectedContext)
  const [statusForm, setStatusForm]  =  useContext(BuahStatusFormContext)

  const handleChange = (event) => {
    let typeOfInput = event.target.name

    switch (typeOfInput) {
      case 'nama':
        setInput({...input,nama:event.target.value})
        break;
      case 'harga':
        setInput({...input,harga:event.target.value})
        break;
      case 'berat':
        setInput({...input,berat:event.target.value})
        break;
      default:break;

    }
  }

  // const handleEdit  = (event) => {
  //   let idBuah = parseInt(event.target.value)
  //   let buah = dataHargaBuah.find(x=> x.id === idBuah)
  //   setinputNama(buah.nama)
  //   setinputHarga(buah.harga)
  //   setinputBerat(buah.berat)
  //   setSelectedId(idBuah)
  //   setStatusForm("edit")
  //
  // }
  //


  const handleSubmit = (event) => {
    event.preventDefault()

    let name = input.nama;
    let price = input.harga;
    let weight = input.berat;

    if (name.replace(/\s/g,'') !== "" && price.toString().replace(/\s/g,'') !== "" && weight.toString().replace(/\s/g,'') !== "" &&  isNaN(price) === false  &&  isNaN(weight) === false ) {

      if (statusForm === "create"){
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name,price,weight})
          .then(res => {
            setBuah([...buah, {id: res.data.id, nama: name,harga:price,berat:weight}])
          })
      }else if(statusForm === "edit"){
        axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`, {name})
        .then(res =>{
            let newDaftarBuah = buah.find(el=> el.id === selectedId)
            newDaftarBuah.nama = name
            newDaftarBuah.harga = price
            newDaftarBuah.berat = weight
            setBuah([...buah])
        })
      }

      setStatusForm("create")
      setSelectedId(0)
      setInput({
        nama:'',
        harga:'0',
        berat:'0'
      })
    }


  }

  return(
    <>
      <h1>Form Daftar Buah</h1>
      <form onSubmit={handleSubmit}>
      <table>
        <tbody>
        <tr>
          <td>Nama Buah</td>
          <td><input name="nama" type="text" value={input.nama} onChange={handleChange}/></td>
        </tr>
        <tr>
          <td>Harga</td>
          <td><input name="harga" type="text" value={input.harga} onChange={handleChange}/></td>
        </tr>
        <tr>
          <td>Berat</td>
          <td><input name="berat" type="text" value={input.berat} onChange={handleChange}/></td>
        </tr>
        <tr>
          <td style={{paddingLeft:"0px"}}><button>submit</button></td>
        </tr>

        </tbody>
        </table>
      </form>
    </>
  )

}

export default BuahForm
