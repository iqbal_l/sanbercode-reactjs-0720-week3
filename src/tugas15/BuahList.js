import React, {useContext} from "react"
import {BuahContext, BuahEditContext, BuahHapusContext} from "./BuahContext"

const BuahList = () =>{
  const [buah,setBuah] = useContext(BuahContext)
  const [handleHapus] = useContext(BuahHapusContext)
  const [handleEdit] = useContext(BuahEditContext)

  return(
    <>
    <h1 align="center">Tugas 15</h1>
    <table style={{border:"1px solid black"}} width="100%">
    <thead>
      <tr>
        <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>No</th>
        <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Nama</th>
        <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Harga</th>
        <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Berat</th>
        <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Aksi</th>
      </tr>
      </thead>
      <tbody>
      {

           buah !== null && buah.map((val, index)=>{
             return(
               <tr key={val.id}>
                 <td style={{backgroundColor:"#FF7F50"}}>{index+1}</td>
                 <td style={{backgroundColor:"#FF7F50"}}>{val.nama}</td>
                 <td style={{backgroundColor:"#FF7F50"}}>{val.harga}</td>
                 <td style={{backgroundColor:"#FF7F50"}}>{val.berat} kg</td>
                 <td style={{textAlign:"center"}}>
                    <button onClick={handleEdit} value={val.id}>Ubah</button>
                    &nbsp;
                    <button onClick={handleHapus} value={val.id}>Delete</button>
                </td>
               </tr>
             )
           })
       }
      </tbody>
    </table>
    </>
  )

}

export default BuahList
