import React, {useContext} from "react";
import { Switch, Link, Route } from "react-router-dom";

import Tugas11 from '../tugas11/Tabel';
import Tugas12 from '../tugas12/Timer';
import Tugas13 from '../tugas13/Tabel';
import Tugas14 from '../tugas14/Tabel';
import Tugas15 from '../tugas15/Tabel';

import {ThemesContext} from "./ThemesContext"

import "./Routes.css"


const Routes = () => {
  const [handleWarna] = useContext(ThemesContext)

  return (
    <>

        <ul id="nav">
          <li>
            <Link to="/">Tugas 11</Link>
          </li>
          <li>
            <Link to="/tugas-12">Tugas 12</Link>
          </li>
          <li>
            <Link to="/tugas-13">Tugas 13</Link>
          </li>
          <li>
            <Link to="/tugas-14">Tugas 14</Link>
          </li>
          <li>
            <Link to="/tugas-15">Tugas 15</Link>
          </li>
          <li><a href="#">Themes</a>
      			<ul>
      				<li><a onClick={handleWarna} warna="default">Default</a></li>
      				<li><a onClick={handleWarna} warna="biru">Biru</a></li>
      				<li><a onClick={handleWarna} warna="hijau">Hijau</a></li>
      			</ul>
      		</li>
        </ul>
      <Switch>

        <Route path="/tugas-12" component={Tugas12}/>
        <Route path="/tugas-13" component={Tugas13}/>
        <Route path="/tugas-14" component={Tugas14}/>
        <Route path="/tugas-15" component={Tugas15}/>
        <Route path="/" component={Tugas11}/>
      </Switch>

    </>
  );
};

export default Routes;
