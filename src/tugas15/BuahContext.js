import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';

export const BuahContext = createContext();
export const BuahHapusContext = createContext();
export const BuahEditContext = createContext();
export const BuahSelectedContext = createContext();
export const BuahStatusFormContext = createContext();
export const BuahInputContext = createContext();

export const BuahProvider = props => {
  const [buah, setBuah] = useState([]);
  const [input, setInput] = useState({nama:'',harga:'0',berat:'0'})
  const [selectedId, setSelectedId]  =  useState(0)
  const [statusForm, setStatusForm]  =  useState("create")

  useEffect( () => {
    if (buah.length === 0){
       axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
       .then(res => {
         setBuah(res.data.map(el => {return {id:el.id,nama:el.name,harga:el.price,berat:el.weight}}))
       })
     }
   })

   const handleHapus = (event) => {
     let idBuah = parseInt(event.target.value)
     let newDaftarBuah = buah.filter(el => el.id != idBuah)

     axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
     .then(res => {
       console.log(res)
     })

     setBuah([...newDaftarBuah])

   }

   const handleEdit  = (event) => {
     let idBuah = parseInt(event.target.value)
     let loadBuah = buah.find(x=> x.id === idBuah)
     setInput({
       nama:loadBuah.nama,
       harga:loadBuah.harga,
       berat:loadBuah.berat
     })
     setSelectedId(idBuah)
     setStatusForm("edit")

   }

  return (
    <>
    <BuahContext.Provider value={[buah, setBuah]}>
      <BuahHapusContext.Provider value={[handleHapus]}>
        <BuahEditContext.Provider value={[handleEdit]}>
          <BuahSelectedContext.Provider value={[selectedId, setSelectedId]}>
            <BuahStatusFormContext.Provider value={[statusForm, setStatusForm]}>
              <BuahInputContext.Provider value={[input, setInput]}>
                {props.children}
              </BuahInputContext.Provider>
            </BuahStatusFormContext.Provider>
          </BuahSelectedContext.Provider>
        </BuahEditContext.Provider>
      </BuahHapusContext.Provider>
    </BuahContext.Provider>
    </>
  );
};
