import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 0,
      date: new Date()
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentDidUpdate(prevProps) {
   if (this.state.time == 0) {
     this.componentWillUnmount()
     document.getElementById('clock-pukul').innerHTML = '';
     document.getElementById('clock-timer').innerHTML = '';
   }
 }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1,
      date: new Date()
    });
  }


  render(){
    return(
      <>
        <h1 align="center">Tugas 12</h1>
        <div style={{float:"left"}} id="clock-pukul">sekarang jam : {this.state.date.toLocaleTimeString()}</div>
        <div style={{float:"right"}} id="clock-timer">hitung mundur: {this.state.time}</div>
      </>
    )
  }
}

export default Timer
