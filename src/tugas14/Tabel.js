import React, { useState, useEffect  } from "react";
import axios from 'axios';

const Tabel = () => {
  const [dataHargaBuah, setDataHargaBuah] = useState([])
  const [inputNama, setinputNama]  =  useState("")
  const [inputHarga, setinputHarga]  =  useState("")
  const [inputBerat, setinputBerat]  =  useState("")
  const [selectedId, setSelectedId]  =  useState(0)
  const [statusForm, setStatusForm]  =  useState("create")


  useEffect( () => {
    if (dataHargaBuah.length === 0){
       axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
       .then(res => {
         setDataHargaBuah(res.data.map(el => {return {id:el.id,nama:el.name,harga:el.price,berat:el.weight}}))
       })
     }
   })

   const handleRefresh = (event) => {
     axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
     .then(res => {
       setDataHargaBuah(res.data.map(el => {return {id:el,nama:el.name,harga:el.price,berat:el.weight}}))
     })
   }

  const handleChange = (event) => {
    let typeOfInput = event.target.name

    switch (typeOfInput) {
      case 'nama':
        setinputNama(event.target.value)
        break;
      case 'harga':
        setinputHarga(event.target.value)
        break;
      case 'berat':
        setinputBerat(event.target.value)
        break;
      default:break;

    }
  }

  const handleEdit  = (event) => {
    let idBuah = parseInt(event.target.value)
    let buah = dataHargaBuah.find(x=> x.id === idBuah)
    setinputNama(buah.nama)
    setinputHarga(buah.harga)
    setinputBerat(buah.berat)
    setSelectedId(idBuah)
    setStatusForm("edit")

  }

  const handleHapus = (event) => {
    let idBuah = parseInt(event.target.value)
    let newDaftarBuah = dataHargaBuah.filter(el => el.id != idBuah)

    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
    .then(res => {
      console.log(res)
    })

    setDataHargaBuah([...newDaftarBuah])

  }

  const handleSubmit = (event) => {
    event.preventDefault()

    let name = inputNama;
    let price = inputHarga;
    let weight = inputBerat;

    if (name.replace(/\s/g,'') !== "" && price.toString().replace(/\s/g,'') !== "" && weight.toString().replace(/\s/g,'') !== "" &&  isNaN(price) === false  &&  isNaN(weight) === false ) {

      if (statusForm === "create"){
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name,price,weight})
          .then(res => {
            setDataHargaBuah([...dataHargaBuah, {id: res.data.id, nama: name,harga:price,berat:weight}])
          })
      }else if(statusForm === "edit"){
        axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`, {name})
        .then(res =>{
            let newDaftarBuah = dataHargaBuah.find(el=> el.id === selectedId)
            newDaftarBuah.nama = name
            newDaftarBuah.harga = price
            newDaftarBuah.berat = weight
            setDataHargaBuah([...dataHargaBuah])
        })


      }


      setStatusForm("create")
      setSelectedId(0)
      setinputNama("")
      setinputBerat("")
      setinputHarga("")
    }

  }

    return (
      <>
      <h1 align="center">Tugas 14</h1>
      <table style={{border:"1px solid black"}} width="100%">
      <thead>
        <tr>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>No</th>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Nama</th>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Harga</th>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Berat</th>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Aksi</th>
        </tr>
        </thead>
        <tbody>
        {

             dataHargaBuah !== null && dataHargaBuah.map((val, index)=>{
               return(
                 <tr key={val.id}>
                   <td style={{backgroundColor:"#FF7F50"}}>{index+1}</td>
                   <td style={{backgroundColor:"#FF7F50"}}>{val.nama}</td>
                   <td style={{backgroundColor:"#FF7F50"}}>{val.harga}</td>
                   <td style={{backgroundColor:"#FF7F50"}}>{val.berat} kg</td>
                   <td style={{textAlign:"center"}}>
                        <button onClick={handleEdit} value={val.id}>Edit</button>
                        &nbsp;
                        <button onClick={handleHapus} value={val.id}>Delete</button>
                    </td>
                 </tr>
               )
             })
         }
        </tbody>
      </table>
        <h1>Form Daftar Buah</h1>
        <form onSubmit={handleSubmit}>
        <table>
          <tbody>
          <tr>
            <td>Nama Buah</td>
            <td><input name="nama" type="text" value={inputNama} onChange={handleChange}/></td>
          </tr>
          <tr>
            <td>Harga</td>
            <td><input name="harga" type="text" value={inputHarga} onChange={handleChange}/></td>
          </tr>
          <tr>
            <td>Berat</td>
            <td><input name="berat" type="text" value={inputBerat} onChange={handleChange}/></td>
          </tr>
          <tr>
            <td style={{paddingLeft:"0px"}}><button>submit</button></td>
          </tr>

          </tbody>
          </table>
        </form>
        </>
    )
}

    export default Tabel
