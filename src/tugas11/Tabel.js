import React, {Component} from "react";
import ListTabel from "./ListTabel";

class Tabel extends Component {
  render(){
    let dataHargaBuah = [
      {nama: "Semangka", harga: 10000, berat: 1000},
      {nama: "Anggur", harga: 40000, berat: 500},
      {nama: "Strawberry", harga: 30000, berat: 400},
      {nama: "Jeruk", harga: 30000, berat: 1000},
      {nama: "Mangga", harga: 30000, berat: 500}
    ]

    return (
      <>
      <h1 align="center">Tugas 11</h1>
      <table style={{border:"1px solid black"}} width="100%">
      <thead>
        <tr>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Nama</th>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Harga</th>
          <th style={{textAlign:"center",backgroundColor:"#AAAAAA"}}>Berat</th>
        </tr>
        </thead>
        <tbody>
        <ListTabel />
        </tbody>
      </table>
      </>
    )
  }
}

export default Tabel
