import React, {Component} from "react";
import IsiTabel from "./IsiTabel";


class ListTabel extends Component {
  render(){
    let dataHargaBuah = [
      {nama: "Semangka", harga: 10000, berat: 1000},
      {nama: "Anggur", harga: 40000, berat: 500},
      {nama: "Strawberry", harga: 30000, berat: 400},
      {nama: "Jeruk", harga: 30000, berat: 1000},
      {nama: "Mangga", harga: 30000, berat: 500}
    ]
    return (
      <>
       {dataHargaBuah.map(el=> {
         return (
           <IsiTabel list={el} />
         )
       })}
     </>
    )
  }
}

export default ListTabel
