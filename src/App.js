import React from 'react';
// import logo from './logo.svg';
import './App.css';
// import Tabel from "./tugas11/Tabel";
// import Timer from "./tugas12/Timer";
//import TabelDynamic from "./tugas13/Tabel";
import {ThemesProvider} from "./tugas15/ThemesContext"
import {BrowserRouter as Router} from "react-router-dom"
import Routes from "./tugas15/Routes";

function App() {
  return (
    <>
      <div style={{margin:"10px auto",padding:"20px",width:"600px"}} id="content">
      <ThemesProvider>
      <Router>
        <Routes/>
      </Router>
      </ThemesProvider>
      </div>
     </>
  );
}

export default App;
